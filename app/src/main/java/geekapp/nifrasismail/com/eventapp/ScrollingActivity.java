package geekapp.nifrasismail.com.eventapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.server.converter.StringToIntConverter;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ScrollingActivity extends AppCompatActivity {

    //Audio Manger to handle audio events
    private AudioManager myAudioManager;
    final static int RQS_1 = 1;



    Button btnCheckIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setContentView(R.layout.activity_scrolling);




        //TODO Enable phone silent
        myAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

        btnCheckIn = (Button)findViewById(R.id.btnCheckIn);
        btnCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView progress = (TextView) findViewById(R.id.progress);
                String updates = "Testing Mode Activated \n";
                progress.setText(updates);
                sleep();
                EditText txttestData = (EditText)findViewById(R.id.testData);
                String testData = txttestData.getText().toString();
                List<String> eventData = Arrays.asList(testData.split(","));
                updates += "Test Data Added \n";
                progress.setText(updates);

                String hour = eventData.get(0).toString();
                String min = eventData.get(1).toString();

                updates += hour + "\n";
                updates += min + "\n";
                progress.setText(updates);


                String mode = eventData.get(2).toString();
                if(mode.equalsIgnoreCase("vib"))
                {
                    makePhoneVibrate();
                    updates += "Vibrate Mode Activated \n";
                    progress.setText(updates);

                }else if(mode.equalsIgnoreCase("sil")) {
                    makePhoneSilent();
                    updates += "Silent Mode Activated \n";
                    progress.setText(updates);

                }


                updates += "Welcome to Meeting you were enrolled \nwaiting for the meeting end time...";
                progress.setText(updates);

                int h = Integer.parseInt(hour);
                int m = Integer.parseInt(min);
                setAlarm(h,m);

            }

        });

    }

    private  void sleep(){
        // Execute some code after 2 seconds have passed
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 2000);
    }
    private void makePhoneVibrate(){
        myAudioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
    }

    private void makePhoneSilent(){
        myAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setAlarm(int hour, int min) {

        Calendar calNow = Calendar.getInstance();
        Calendar calSet = (Calendar) calNow.clone();

        calSet.set(Calendar.HOUR_OF_DAY, hour);
        calSet.set(Calendar.MINUTE, min);
        calSet.set(Calendar.SECOND, 0);
        calSet.set(Calendar.MILLISECOND, 0);

        if (calSet.compareTo(calNow) <= 0) {
            // Today Set time passed, count to tomorrow
            calSet.add(Calendar.DATE, 1);
        }



        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                getBaseContext(), RQS_1, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calSet.getTimeInMillis(),
                pendingIntent);

    }


}
