package geekapp.nifrasismail.com.eventapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by NifrasIsmail on 6/25/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {
    private  AudioManager manager;
    @Override
    public void onReceive(Context k1, Intent k2) {
        // TODO Auto-generated method stub

        Toast.makeText(k1, "Event Ended \n Normal Mode Activated \n Thank you for your attendance.", Toast.LENGTH_LONG).show();
        manager = (AudioManager)k1.getSystemService(Context.AUDIO_SERVICE);
        manager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);


    }

}
